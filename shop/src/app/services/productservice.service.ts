import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";

import { tap } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class ProductserviceService {

  constructor(
    private http: HttpClient,
  ) { }
  getproduct() {
    return this.http
      .get<any>("http://127.0.0.1:8001/api/getall")
      .pipe(
        tap((product) => {
          return product;
        })
      );
  } 
  orders(data: any) {
   
    return this.http.post(
      "http://127.0.0.1:8001/api/order",
      {
        id: data.id,
        dish: data.dish,
        price: data.price,
        available: data.available,
      
      },
    
    );
  }
}
